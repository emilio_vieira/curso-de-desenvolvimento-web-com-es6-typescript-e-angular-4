import Veiculo from "./Veiculo";

class Moto extends Veiculo{

  constructor(){
    super();
  }

  /**
   * Overide Method - Sobrescrita de metodo
   */
  public acelerar(): void {
    this.velocidade = this.velocidade + 20;
  }

}
// Outro tipo de exportação. Para explanar melhor
export default Moto;