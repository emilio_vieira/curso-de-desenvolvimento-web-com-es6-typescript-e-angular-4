import Carro from './Carro';

export default class Concessionaria implements IConcessionaria{
  private endereco: string;
  private listaDeCarros: Array<Carro> = [];

  constructor(endereco:string, listaDeCarros: Array<Carro>){
    this.endereco = endereco;
    this.listaDeCarros = listaDeCarros;
  }

  imprimirHorarioDeFuncionamento():string {
    return "Segunda a Sexta, das 8:00 até 17:00";
  }

  public fornecerEndereco():string {
    return this.endereco;
  }

  public mostrarListaDeCarros(): Array<Carro> {
    return this.listaDeCarros;
  }
}

export const fabrica = "São Gonçalo";