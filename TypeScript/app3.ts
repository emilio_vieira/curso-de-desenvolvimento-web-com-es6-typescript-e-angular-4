import Pessoa from "./classes/Pessoa";
import Carro from "./classes/Carro"
import Concessionaria, {fabrica} from "./classes/Concessionaria"
import Moto from "./classes/Moto";

let carro = new Carro('Veloster',3);

let moto = new Moto();

console.log(carro);
carro.acelerar();
console.log('Acelerou',carro);

console.log(moto);
moto.acelerar();
console.log('Acelerou',moto);
console.log(moto);

let concessionaria = new Concessionaria('av.',[carro]);
concessionaria.imprimirHorarioDeFuncionamento();