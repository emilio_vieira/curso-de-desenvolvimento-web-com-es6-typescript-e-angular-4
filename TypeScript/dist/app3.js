"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Carro_1 = __importDefault(require("./classes/Carro"));
var Concessionaria_1 = __importDefault(require("./classes/Concessionaria"));
var Moto_1 = __importDefault(require("./classes/Moto"));
var carro = new Carro_1.default('Veloster', 3);
var moto = new Moto_1.default();
console.log(carro);
carro.acelerar();
console.log('Acelerou', carro);
console.log(moto);
moto.acelerar();
console.log('Acelerou', moto);
console.log(moto);
var concessionaria = new Concessionaria_1.default('av.', [carro]);
concessionaria.imprimirHorarioDeFuncionamento();
