"use strict";
var ola = function (nome) {
    console.log("Olá" + nome);
};
ola("Emilio");
// inferencia de tipos
var mensagem = "Seja bem vindo!!!";
var temporadasFriends = 10;
var estudandoAngular = true;
var listaDeFrutas = ['Uva', 'Banana', 'Abacaxi'];
var notasDasProvas = [7.5, 8, 9];
