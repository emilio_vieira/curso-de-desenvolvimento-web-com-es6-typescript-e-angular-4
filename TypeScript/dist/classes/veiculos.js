"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Carro = /** @class */ (function () {
    function Carro(modelo, portas) {
        this.velocidade = 0;
        this.modelo = modelo;
        this.numeroDePortas = portas;
    }
    Carro.prototype.acelerar = function () {
        this.velocidade = this.velocidade + 10;
    };
    Carro.prototype.parar = function () {
        this.velocidade = 0;
    };
    Carro.prototype.velocidadeAtual = function () {
        return Number(this.velocidade);
    };
    Carro.prototype.getModelo = function () {
        return this.modelo;
    };
    return Carro;
}());
exports.default = Carro;
