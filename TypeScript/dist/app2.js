"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Pessoa_1 = __importDefault(require("./classes/Pessoa"));
var Carro_1 = __importDefault(require("./classes/Carro"));
var Concessionaria_1 = __importStar(require("./classes/Concessionaria"));
// let concessionaria = new Concessionaria('Av. Paulista');
// console.log(concessionaria);
console.log('Fabricados em ', Concessionaria_1.fabrica);
var pessoa = new Pessoa_1.default('Emilio', 'Onix');
// console.log(pessoa.dizerCarroPreferido());
var carro = new Carro_1.default('Versa', 4);
pessoa.comprarCarro(carro);
/* -- Criar Carros -- */
var carroA = new Carro_1.default('Versa', 4);
var carroB = new Carro_1.default('Onix', 4);
var carroC = new Carro_1.default('Voyage', 4);
/* montar lista de carros da concessionaria */
var listaDeCarros = [carroA, carroB, carroC];
var concessionaria = new Concessionaria_1.default('Av. Brasil', listaDeCarros);
// console.log(concessionaria.mostrarListaDeCarros());
/*-- comprar o carro --*/
var cliente = new Pessoa_1.default('Emilio', 'Versa');
// console.log(cliente.dizerCarroPreferido());
concessionaria.mostrarListaDeCarros().map(function (carro) {
    if (carro.getModelo() === cliente.dizerCarroPreferido()) {
        cliente.comprarCarro(carro);
    }
});
console.log('cliente.dizerCarroQueTem() ->', cliente.dizerCarroQueTem());
