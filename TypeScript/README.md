# EcmmaScript - ES6

## TypesScript

1. Instalação do Typescript
1. Configuração - tsconfig.json
1. Inferência de Tipos
1. Verificação estática
1. Suporte a classes e interfaces

----

## Instalação do TypeScript

```npm
npm install -g typescript
```

- verificar instalação

```tsc
"tsc -v"
```

----

### Configuracão do TypeScript

#### TSCONFIG.JSON

Iniciando arquivo tsconfig.json

```tsconfig.json
tsc -init
```

Configurando caminho para arquivos transpilados(.js)
outDir - home dos arquivos compilados. 

```ex
outDir: './dist'
```

tsc -w - Assistindo os processos de transfile e salvando na pasta dist, configurada no tsconfig.json em outDir.

----

### Transpilação - Transformar ES-6 em ES-5

#### Executando typescript e fazendo um transpile

- tsc [arquivo][.ts]? => transforma em "app.js"

```tsc
tsc app
node app.js
```

##### Transpile - Transformação do Código Atual ES-6 para a versão antiga do JS- ES-5. Aceita pelos navegadores

----

#### Inferência de Tipos

```tipagem
let ola = (nome: string)=>{
    console.log("Olá" + nome);



3
3};

ola("Emilio");
```

##### Tipagem

```tipagem
let mensagem:string = "Seja bem vindo!!!";
let temporadasFriends:number = 10;
let estudandoAngular:boolean = true;
let listaDeFrutas:Array<string> = ['Uva', 'Banana', 'Abacaxi'];
let notasDasProvas:Array<number>= [7.5, 8, 9];
// Outra forma de declaracao de arrays de tipos
let listaDeLegumes: string[]
```

----

### Suporte a classes e interfaces

#### Criando e instanciando Classes

1. Criar app2.ts
1. Class é exclusivo do ES-6
1. Criarmos as classes Carro e Concessionaria
1. Tipo any ou qualquer tipo de dados;

#### Módulos

1. Dividir as classes em arquivos isolado. (carro.ts,concessionaria.ts)

1. Usar "export" para permitir uso especifico do pacote/importado

1. usando o "import" para adicionar as classes em app2.ts

1. criar uma "constante" no arquivo e dar "export" na mesma.

```export
export const fabrica = "São Gonçalo";
```

1. Importar constante com palavra reservada "as" de aliases.

1. Exportação "DEFAULT". importa sem abre/fecha chaves.

```export
export default class Carro
import Carro, { fabrica as fab }
```

1. Importar classes para tipagem por outras classes.

----

#### Herança

1. criar veiculo.ts e moto.ts
1. clonar arquivo carro para veiculo
1. dar extends da classe 'Veiculo' na classe 'Carro'
1. Super() será exigido;
1. Pode ser interessante colocar o operador export default no fim. Outro tipo de exportação. Para explanar melhor.

```outro-export
import Veiculo from "./Veiculo";

class Moto extends Veiculo{

  constructor(){
    super();
  }

}
// Outro tipo de exportação. Para explanar melhor
export default Moto;
```

#### Sobrescrita de Métodos

#### Interfaces


