import Pessoa from "./classes/Pessoa";
import Carro from "./classes/Carro"
import Concessionaria, {fabrica} from "./classes/Concessionaria"


// let concessionaria = new Concessionaria('Av. Paulista');
// console.log(concessionaria);
console.log('Fabricados em ',fabrica);

let pessoa = new Pessoa('Emilio','Onix');
// console.log(pessoa.dizerCarroPreferido());

let carro = new Carro('Versa', 4);

pessoa.comprarCarro(carro);


/* -- Criar Carros -- */
let carroA = new Carro('Versa', 4);
let carroB = new Carro('Onix', 4);
let carroC = new Carro('Voyage', 4);

/* montar lista de carros da concessionaria */
let listaDeCarros: Array<Carro> = [carroA,carroB,carroC];
let concessionaria = new Concessionaria('Av. Brasil', listaDeCarros);

// console.log(concessionaria.mostrarListaDeCarros());

/*-- comprar o carro --*/
let cliente = new Pessoa('Emilio','Versa');
// console.log(cliente.dizerCarroPreferido());

concessionaria.mostrarListaDeCarros().map( (carro: Carro)=>{

    if(carro.getModelo() === cliente.dizerCarroPreferido()){
      cliente.comprarCarro(carro);
    }
    
});

console.log('cliente.dizerCarroQueTem() ->',cliente.dizerCarroQueTem());