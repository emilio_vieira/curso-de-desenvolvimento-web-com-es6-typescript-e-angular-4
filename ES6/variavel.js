/**
 * Diferença entr VAR e LET
 * 
 * var - predonina no escopo global, e declarada dentro de outros escopos(if,for), pode ser vista for deles.
 * 
 * 1# Se criar uma variavel com VAR dentro do IF, 
 * ele cria uma proxy. Elevado/Colocando a varial pra fora do escopo de chaves do IF.
 * 
 * 2# Se Criarmos uma variavel com LET; no escopo global(topo)  
 * e dentro de um outro escopo(IF,FOR), logo abaixo, criarmos outra com mesmo nome, 
 * usando o VAR, um erro será lançado(exception).
 * 
 * 3# CONST - Declarar constantes. NAO MODIFICAVEIS!
 * Nao tera seu conteudo modificado ao longo do carregamento do script.
 */

 //////////////////////////////////
 // 1# ESCOPO VAR E LET
 //////////////////////////////////
var serie = "Punisher";
if(true){
    let serie = "Game of thrones";
    console.log('LET',serie);
}
console.log('VAR',serie);


//////////////////////////////////
// 2# ERRO
//////////////////////////////////
let outraSerie="Better Call Saul";
if(true){
    /** 
     * Ao descomentar está linha, um erro será lançando no console: 
     * Identifier 'outraSerie' has already been declared 
     */
     // var outraSerie="Demolidor";
}
console.log('LET',outraSerie);


//////////////////////////////////
// 3# CONSTANTES
//////////////////////////////////
const maisOutraSerie = 'Good Place';

/** 
 * Ao descomentar está linha, um erro será lançando no console: 
 * Constantes nao podem ser modificadas. Espera-se mesmo o valor inicial
 */
// Erro : TypeError: Assignment to constant variable.
// maisOutraSerie = 'Divorce';

console.log('CONST',maisOutraSerie);