
// var dobroDoValor = function(numero){
//     return numero * 2;
// }

/** Arrow => Flexa 
 * 
 * NAO Precisamos dos parenteses:
 *  - Quando houver apenas 1 parametro.
 * 
 * Precisamos dos parenteses:
 *  - Se nao houver parametros. OU,
 *  - Para multiplos parametros.
 * 
*/

// Var Functions / Closures
var dobroDoValor = function(numero){
    return numero * 2;
}

// Arrow Function simples com 1 param sem retorno direto
var dobroDoValor = numero => {
    let numRet = numero * 2
    return numRet;
}

// Clean Returns - retorno direto com 1 param
var dobroDoValor = numero => numero * 2;

// Arrow Functions com Multiplos parametros
var MultiplicaDoValor = (numero1,numero2) => {
    return numero1 * numero2;
}

// Arrow Functions sem parametros
var MultiplicaDoValor = () => {
    return new String('Sem params');
}


console.log(dobroDoValor(7));